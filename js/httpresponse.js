/**
 * @author waar0003
 */

/*
 * Definition of result type datastructures. Each code is divided into different types, which is designated by the first number of the code:
 * 1xx: Informational - Request received, continuing process
 * 2xx: Success - The action was successfully received, understood, and accepted
 * 3xx: Redirection - Further action must be taken in order to complete the request
 * 4xx: Client Error - The request contains bad syntax or cannot be fulfilled
 * 5xx: Server Error - The server failed to fulfill an apparently valid request
 */
var typeInfo = {
	name: "Informational",
	description: "Request received, continuing process"
};

var typeSuccess = {
	name: "Success",
	description: "The action was successfully received, understood, and accepted"
};

var typeRedir = {
	name: "Redirection",
	description: "Further action must be taken in order to complete the request"
};

var typeClientError = {
	name: "Client Error",
	description: "The request contains bad syntax or cannot be fulfilled"
};

var typeServerError = {
	name: "Internal Server Error",
	description: "The server failed to fulfill an apparently valid request"
};
var typeNotfound = {
	name: "404 not found",
	description: "The server has not found anything matching the URI given"
};

/**
 * Returns a datastructure that explains the specified response code, or NULL is the code is not recognized
 * @param {Object} responseCode the response code that should be explained
 * @return a datastructure that explains the specified response code, or NULL is the code is not recognized
 */
function getHTTPResponseInfo(responseCode) {
	var result = {};
	result.code = responseCode;
	if (responseCode == 200 ) {
		result.type = typeSuccess;
		result.description = "OK";
	}
	if (responseCode == 404 ) {
		result.type = typeNotfound;
		result.description = "Not found";
	}
	if (responseCode == 302 ) {
		result.type = typeRedir;
		result.description = "Redirection";
	}
	if (responseCode == 500 ) {
		result.type = typeServerError;
		result.description = "Internal Server Error";
	}
	return result;
}

function check() { // Function that reacts to the trigger of a button on the HTML page.

//Function that gives the ouput of the HTTPResponse function	
		var print = function(o){
    var str='<div class="httpoutput">';

    for(var p in o){
        if(typeof o[p] == 'string'){
            str+= p + ': ' + o[p]+' </br>';
        }else{
            str+= p + ': </br>' + print(o[p]) + '</div>';
        }
    }

    return str;
}


		var httpresponse = document.getElementById("httpcode").value;
		document.getElementById('outputresponse').innerHTML = print(getHTTPResponseInfo(httpresponse));
		
	}