function check() {
	// Checks the selected radiobutton
	var checked = null;
	var inputs = document.getElementsByName('choice');
	for (var i = 0; i < inputs.length; i++) {
		if (inputs[i].checked) {
			checked = inputs[i];
			break;
		}

	}

	if (checked == null) {

	} else {

		var computerChoice = Math.random();

		if (computerChoice < 0.34) {
			computerChoice = "rock";

		} else if (computerChoice <= 0.67) {
			computerChoice = "paper";

		} else {
			computerChoice = "scissors";
		}

		var compare = function(choice1, choice2) {

			if (choice1 === choice2) {

				return "The result is a tie!";

			} else if (choice1 === "rock") {

				if (choice2 === "scissors") {

					return "rock wins";

				} else {

					return "Scissors wins";

				}

			} else if (choice1 === "paper") {

				if (choice2 === "scissors") {

					return "Scissors wins";

				} else {

					return "Paper wins";

				}

			} else if (choice1 === "scissors") {

				if (choice2 === "paper") {

					return "Scissors wins";

				} else {

					return "Paper wins";

				}

			}

		};
		var output = compare(checked.value, computerChoice);  //Compares to output of the user with the ouput of the computer.
		document.getElementById('output').innerHTML = output; // Puts the output in the output div.

		document.getElementById('user').innerHTML = checked.value; // Puts the userinput in the userinput div
		document.getElementById('computer').innerHTML = computerChoice; // Puts the computerinput in the computerinput div
	}
}
